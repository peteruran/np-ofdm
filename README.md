# Spectrum Sensing in OFDM Cognitive Radios

Spectrum Sensing for OFDM Cognitive Radios using a Neyman-Pearson detector.

An interactive notebook is provided to test the detector.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/peteruran%2Fnp-ofdm/master?filepath=notebook.ipynb)