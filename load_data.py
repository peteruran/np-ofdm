#!/usr/bin/python3
import numpy as np
from scipy import io

## Load all data

# 1024 realizations of PU data symbols S(k), gaussion and bpsk
bpsk_pu = np.array(io.loadmat('data/T1_data_Sk_BPSK.mat')['T1_data_Sk_BPSK']).T
gaussian_pu = io.loadmat('data/T1_data_Sk_Gaussian.mat')['T1_data_Sk_Gaussian']
# 1024 realizations of PU signal and SU noise
s_n = io.loadmat('data/T3_data_sigma_s.mat')['s_t']
w_n = io.loadmat('data/T3_data_sigma_w.mat')['w']
# 1024 realizations of signal observed at the SU when H0 and H1
x_h0 = io.loadmat('data/T3_data_x_H0.mat')['T3_data_x_H0']
x_h1 = io.loadmat('data/T3_data_x_H1.mat')['T3_data_x_H1']
# 100 realizations of signal observed at the SU x(n) to determine spectrum vacancy. Sample size is N = 256.
# Noise variance 1 and signal power 5
x_n = io.loadmat('data/T8_numerical_experiment.mat')['T8_numerical_experiment']
