#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal
import scipy.stats as stats
from scipy import constants
from scipy.integrate import simps

import os
import time
import sys

## Set working directory to script location
os.chdir(os.path.dirname(sys.argv[0]))

## Load all data
from load_data import *



## Check that signals are indeed gaussian i.i.d
def check_gaussian(samples, data_name):
	X_k = np.fft.ifftshift(samples) # shift zero frequency to first index
	x_n = np.fft.ifft(X_k, axis=0)

	x_r = np.real(x_n)
	x_i = np.imag(x_n)

	## Plots

	# define size of figure and axes
	left, width = 0.1, 0.65
	bottom, height = 0.1, 0.65
	bottom_h = left_h = left + width + 0.02

	rect_scatter = [left, bottom, width, height]
	rect_histx = [left, bottom_h, width, 0.2]
	rect_histy = [left_h, bottom, 0.2, height]

	# define rectangular figure
	fig = plt.figure(1, figsize=(8, 8))

	# define axes
	axScatter = plt.axes(rect_scatter)
	axHistx = plt.axes(rect_histx)
	axHisty = plt.axes(rect_histy)
	axHistx.set_title('Scatter plot and histograms for ' + data_name)
	axHistx.get_xaxis().set_visible(False)
	axHisty.get_yaxis().set_visible(False)

	# define scatter plot
	axScatter.scatter(x_r, x_i)
	axScatter.set_xlabel('Real part')
	axScatter.set_ylabel('Imaginary part')

	# define histograms
	bins = 20
	axHistx.hist(x_r, bins=bins)
	axHisty.hist(x_i, bins=bins, orientation='horizontal')

	plt.show()


## Takes normal disributed data and transforms it to a chi square distribution
def norm_to_chi2(data, N):
	var = np.var(data.real) + np.var(data.imag) # technically equal, since real and imag are IID normal distributions
	chi2 = 2 * (data.real**2 + data.imag**2) / var # transform normal distributed samples to chi2 distribution
	# generate new distribution for N-sample detector
	if N != 1:
		chi2 = np.array([ chi2[n:n+N].sum() for n in range(0, chi2.size-N) ])

	chi2_var = np.var(chi2) # variance of chi2 distribution
	return (chi2_var, chi2)


## Generate detector performance for a given N
def detector_performance(N, alpha=False):

	h0_var, h0_model = norm_to_chi2(np.array(x_h0), N)
	h1_var, h1_model = norm_to_chi2(np.array(x_h1), N)

	## Generate PDF for H0 and H1 from estimated variance
	x = np.linspace(0, 4*N, 1000)
	h0_pdf = stats.chi2.pdf(x, df=h0_var/2) # variance is 2*df
	h1_pdf = stats.chi2.pdf(x, df=h1_var/2)

	print('H0 degrees of freedom', np.around(h0_var/2, 2), ', H1 degrees of freedom', np.around(h1_var/2, 2))

	# getting the CDF eliminates the need to integrate
	h0_cdf = stats.chi2.cdf(x, df=h0_var/2)
	h1_cdf = stats.chi2.cdf(x, df=h1_var/2)

	## Determine specific detector performace if alpha is set
	if alpha:
		lmda = stats.chi2.ppf(1-alpha, df=h0_var/2)
		beta  = stats.chi2.cdf(lmda, df=h0_var/2)
		print('P_D =', np.around(beta,2), 'for a given P_FA =', np.around(alpha,2), 'and N =', N)

	else:
		## Find the lambda from the intersection point of the PDFs
		idx = np.argwhere(np.diff(np.sign(h0_pdf - h1_pdf))).flatten()
		if idx.any(): # return true if any element evaluates to True
			idx = idx[np.argmax(idx)] # return index of largest number, due to small oscillations in function
			lmda = x[idx]
			print('Lambda:', np.around(lmda,2))

			## Find P_FA and P_D from CDFs
			p_fa = 1 - h0_cdf[idx]
			p_d = 1 - h1_cdf[idx]

			print('P_D =', np.around(p_d,2), 'and P_FA =', np.around(p_fa,2), 'for N =', N)

		else:
			print('Lambda not found. No intersection between H0 and H1.')
			idx = False


	## Plots
	fig,(ax1, ax2) = plt.subplots(2, 1)
	ax1.set_title('Performance of N-sample detector using ' + str(N) + ' samples')
	bins = 15
	ax1.hist(h0_model, bins=bins, label='Model for H0', histtype='step')
	ax1.hist(h1_model, bins=bins, label='Model for H1', histtype='step')
	ax1.legend(loc='upper right')
	ax1.set_xlabel('')
	ax1.grid()

	## More plots
	ax2.set_title('Generated PDFs')
	ax2.plot(x, h0_pdf, label='PDF for H0')
	ax2.plot(x, h1_pdf, label='PDF for H1')
	if lmda:
		ax2.axvline(lmda, color='r')
	ax2.legend(loc='upper right')

	ax2.set_xlabel('')
	ax2.grid()

	plt.tight_layout()
	plt.show()


## Generated the Receiver Operating Characteristic (ROC) for two given variances in a chi2 PDF
def get_roc(h0_var, h1_var):
	p_fa = np.linspace(0,1,100)
	h0_ppf = stats.chi2.ppf(1-p_fa, df=h0_var/2)
	p_d = 1-stats.chi2.cdf(h0_ppf, df=h1_var/2)
	return (p_fa, p_d)


##  Generates all ROCs up to a given limit N
def compute_all_rocs(N, res=10):
	fix, ax = plt.subplots(1,1)
	for n in [res*n for n in range(1,N) if 10*n<=N]:
		h0_var, h0_model = norm_to_chi2(np.array(x_h0), n)
		h1_var, h1_model = norm_to_chi2(np.array(x_h1), n)
		#rocs[n] = get_roc(h0_var, h1_var)
		p_fa, p_d = get_roc(h0_var, h1_var)
		ax.plot(p_fa, p_d, label='N='+str(n))

	ax.set_title('Receiver-operating-curve (ROC) for N=' + str(N))
	ax.set_xlabel('P_FA')
	ax.set_ylabel('P_D')
	ax.legend(loc='upper right')
	ax.grid()

	plt.tight_layout()
	plt.show()


## Finds the minimal needed N for a given alpha and beta. Not used.
def get_minimal_N(h0_var, h1_var, alpha, beta):
		N = 1/np.power(h1_var-h0_var,2) * np.power( stats.norm.ppf(beta)*h1_var - stats.norm.ppf(alpha)*h0_var, 2 )
		return N


## Compares P_FA and P_D for chi2 and a normal approximation for a given N
def compare_chi2_norm(N):
	h0_var, h0_model = norm_to_chi2(np.array(x_h0), N)
	h1_var, h1_model = norm_to_chi2(np.array(x_h1), N)

	chi2_p_fa, chi2_p_d = get_roc(h0_var, h1_var)
	lmda = stats.chi2.ppf(1-chi2_p_fa, df=h0_var/2)

	norm_p_fa = 1 - stats.norm.cdf(lmda, loc=h0_var/2, scale=np.sqrt(h0_var))
	norm_p_d = 1 - stats.norm.cdf(lmda, loc=h1_var/2, scale=np.sqrt(h1_var))

	fix, ax = plt.subplots(1,1)
	ax.plot(lmda, chi2_p_fa, label='Chi2 P_FA')
	ax.plot(lmda, chi2_p_d, label='Chi2 P_D')
	ax.plot(lmda, norm_p_fa, label='Norm P_FA')
	ax.plot(lmda, norm_p_d, label='Norm P_D')
	ax.legend(loc='upper right')
	ax.set_xlabel('Lamda')

	plt.tight_layout()
	plt.show()


## The actual Neuman-Pearson detector
def detector(data, var_s, var_w, p_fa, N):
	h0_var = var_w
	h1_var = var_s+var_w
	# Find lambda' using normal distribution
	lmda = stats.norm.ppf(1-p_fa, loc=N*h0_var, scale=np.sqrt(h0_var))
	p_d  = stats.norm.cdf(lmda, loc=N*h0_var, scale=np.sqrt(h0_var))
	print('Given N =', N, 'and P_FA =', p_fa, '. This detector will have P_D =', np.round(p_d,2))

	counter = 0
	true_counter = 0
	for samples in data.T:
		counter += 1
		test_statistic = (np.abs(samples[:N])**2).sum()
		if test_statistic > lmda:
			true_counter += 1 # increment true_counter
		print('Case:', counter, test_statistic > lmda)
	print('Detector found', true_counter, 'positive cases and', counter-true_counter, 'negative cases.')

## Main function
if __name__ == "__main__":

	## Task 1
	#check_gaussian(bpsk_pu, "BPSK")
	#check_gaussian(gaussian_pu, "Gaussian")

	## Task 3
	#detector_performance(N=1)

	## Task 4
	#detector_performance(N=100, alpha=0.1)

	## Task 5
	#compute_all_rocs(100)

	## Task 6
	#compare_chi2_norm(N=100)

	## Task 8
	#detector(data=x_n, var_s=1, var_w=5, p_fa=0.1, N=256)
	#detector(data=x_n, var_s=1, var_w=5, p_fa=0.01, N=256)
